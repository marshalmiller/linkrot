black==22.3.0
coverage==6.4.1
flake8==4.0.1
mccabe<=0.7.0
mypy==0.961
pylint==2.14.1
pytest==7.1.2
